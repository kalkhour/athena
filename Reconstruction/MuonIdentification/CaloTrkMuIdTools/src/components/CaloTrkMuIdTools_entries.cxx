#include "CaloTrkMuIdTools/CaloMuonLikelihoodTool.h"
#include "CaloTrkMuIdTools/CaloMuonScoreTool.h"
#include "CaloTrkMuIdTools/CaloMuonTag.h"
#include "CaloTrkMuIdTools/TrackDepositInCaloTool.h"
#include "CaloTrkMuIdTools/TrackEnergyInCaloTool.h"

DECLARE_COMPONENT(CaloMuonTag)
DECLARE_COMPONENT(TrackEnergyInCaloTool)
DECLARE_COMPONENT(TrackDepositInCaloTool)
DECLARE_COMPONENT(CaloMuonLikelihoodTool)
DECLARE_COMPONENT(CaloMuonScoreTool)
